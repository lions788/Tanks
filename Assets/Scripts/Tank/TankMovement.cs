using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement : MonoBehaviour
{
    public float speed = 1500f; // How fast the tank moves forward and back
	public float turnSpeed = 180f; // ow fast the tank turns in degrees per second

	public GameObject tankRenderer; // The mesh of the tank which is what's being tilted

	private Rigidbody rigidBody;
	private float movementInputValue; // The current value of the movement input
	private float turnInputValue; // The current value of the turn input

	private Vector3 rotationTarget = new Vector3(0, 0, 9); // The strength of the tilt

	private void Awake() {
		rigidBody = GetComponent<Rigidbody>();
	}

	private void OnEnable() {
		// When the take is turned on, make sure it is not kinematic
		rigidBody.isKinematic = false;

		// Resets the Input Values
		movementInputValue = 0f;
		turnInputValue = 0f;
	}

	private void OnDisable() {
		// When the tank is turned off, set it to kinematic so it stops moving
		rigidBody.isKinematic = true;
	}

	private void Update() {
		movementInputValue = Input.GetAxisRaw("Vertical");
		turnInputValue = Input.GetAxis("Horizontal");
	}

	private void FixedUpdate() {
		Move();
		Turn();
	}

	private void Move() {

		// Create a vector in the direction the tank is facing with a magnitude
		var velocity = transform.forward * (movementInputValue * speed * Time.fixedDeltaTime);
		rigidBody.AddForce(velocity);
	}

	private void Turn() {
		// Determine the number of degrees to be turned based on the input
		// Speed and time between frames
		float turn = turnInputValue * turnSpeed * Time.deltaTime;

		// Make this into a rotation in the y-axis
		Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

		// Apply this rotation to the rigidbody's rotation
		rigidBody.MoveRotation(rigidBody.rotation * turnRotation);
		
		
		// Tilting
		if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.5) {

			// It's a Quaternion, what do you want me to say about this, I don't know how it works, it just does!
			tankRenderer.transform.localRotation = Quaternion.Lerp(tankRenderer.transform.localRotation, Quaternion.Euler(rotationTarget * turnInputValue), Time.fixedDeltaTime * 7);
		}
		else if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.3) {

			// It's a Quaternion, what do you want me to say about this, I don't know how it works, it just does!
			tankRenderer.transform.localRotation = Quaternion.Lerp(tankRenderer.transform.localRotation, Quaternion.Euler(0,0,0), Time.fixedDeltaTime * 7);
		}
	}
}
