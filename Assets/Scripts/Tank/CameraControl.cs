using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
	public float DampTime = 0.4f;
	public float Zoom = 5;
	public float MaxZoom = 15;
	public float MinZoom = 2;

	public Transform Target;

	private Vector3 moveVelocity;
	private Vector3 desiredPosition;
	private Quaternion desiredRotation;

	private void Awake() {
		//Target = GameObject.FindGameObjectWithTag("Player").transform;
	}

	private void FixedUpdate() {
		MoveAuto();
	}

	private void Update() {
		if (Zoom >= MinZoom && Zoom <= MaxZoom) {
			float scroll = Input.GetAxis("Mouse ScrollWheel");
			Zoom += scroll;
			Camera.main.orthographicSize = Zoom;
			Target.position -= new Vector3(0, 0, scroll);
		}
	}

	private void MoveAuto() {
		desiredPosition = Target.position;
		transform.position = Vector3.SmoothDamp(transform.position,
		desiredPosition, ref moveVelocity, DampTime);
		desiredRotation = Target.rotation;
		transform.rotation = Quaternion.Lerp(transform.rotation,
		desiredRotation, DampTime);
	}

	private void MoveManual() {
		
	}
}