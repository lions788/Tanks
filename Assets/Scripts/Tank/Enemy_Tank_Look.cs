using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Tank_Look : MonoBehaviour
{
	public Transform Player;
	private void Update() {
		transform.LookAt(Player.transform.position);
	}
}
