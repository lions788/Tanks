using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EnemyTankShooting : MonoBehaviour
{
    // Prefab of the shell
	public GameObject shell;
	// A Child of the tank where the shells are spawned
	public Transform fireTransform;

	// The force given to the shell when firing
	public float launchForce = 30f;
	public float shootDelay = 1f;

	private bool canShoot;
	private float shootTimer;
	private void Awake() {
		canShoot = false;
		shootTimer = 0;
	}

	private void Update() {
		if (canShoot == true) {
			shootTimer -= Time.deltaTime;
			if (shootTimer <= 0) {
				shootTimer = shootDelay;
				Fire();
			}
		}
	}

	private void Fire() {
		// Create an instance of the shell and store a reference to it's rigidbody
		GameObject shellInstance = Instantiate(shell, fireTransform.position, fireTransform.rotation);

		// Set the shell's velocity to the launch force in the fire position's forwasrd direction
		shellInstance.GetComponent<Rigidbody>().velocity = launchForce * fireTransform.forward;
		shellInstance.GetComponent<Shell>().sender = gameObject;
	}

	private void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			canShoot = true;
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			canShoot = false;
		}
	}
}
