using UnityEngine;

public class Toot : MonoBehaviour
// TOOT
{
	public AudioSource tooter;
    void Update()
    {
		if (Input.GetKeyDown("space")) {
			tooter.PlayOneShot(tooter.clip);
		}
    }
}
