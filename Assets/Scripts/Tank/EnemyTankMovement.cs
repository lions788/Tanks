using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyTankMovement : MonoBehaviour
{

	// The tank will stop moving towards the player once it reaches this distance
	public float closeDistance = 8f;
	public float baseCloseDistance = 8f;
	// The tank's turret object
	public Transform turret;

	// A reference to the player - this will be set when the enemy is loaded
	private GameObject player;
	// A reference to the nav mesh agent component
	private NavMeshAgent navAgent;
	// A reference to the rigidbody component
	private Rigidbody rigidBody;


	// Will be set to true when this tank should follow the player 
	private bool follow;
	private bool returnToBase;

	// A reference to the base location
	public GameObject Base;

    // Update is called once per frame
    void Update()
    {
        if (follow == true) {
			
			// Get distance from player to enemy tank
			float distance = (player.transform.position - transform.position).magnitude;

			// If distance is less than stop distance, then stop moving
			if (distance > closeDistance) {
				navAgent.SetDestination(player.transform.position);
				navAgent.isStopped = false;
			}
			else {
				turret.LookAt(Base.transform.position);
				navAgent.isStopped = true;
			}

			if (turret != null) {
				turret.LookAt(player.transform);
			}
		}

		// Returns to base after player is not in range
		else if (returnToBase == true) {
			float toBaseDistance = (Base.transform.position - transform.position).magnitude;
			if (toBaseDistance > baseCloseDistance) {
				navAgent.SetDestination(Base.transform.position);
				navAgent.isStopped = false;
			}
			else {
				navAgent.isStopped = true;
			}


			if (turret != null) {
				turret.LookAt(Base.transform.position);
			}
		}
    }

	private void Awake() {

		player = GameObject.FindGameObjectWithTag("Player");
		navAgent = GetComponent<NavMeshAgent>();
		rigidBody = GetComponent<Rigidbody>();
		follow = false;
		returnToBase = true;
	}

	private void OnEnable() {
		// When the tank is turned on, make sure it is not kinematic
		rigidBody.isKinematic = false;
	}

	private void OnDisable() {
		//When the tank is turned off, make sure it is kinematic
		rigidBody.isKinematic = true;
	}

	private void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			follow = true;
			returnToBase = false;
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			follow = false;
			returnToBase = true;
		}
	}
}
