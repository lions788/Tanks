using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankHealth : MonoBehaviour
{
    // The amount of health each tank starts with
	public float startingHealth = 100f;

	// A prefab that will be instantiated in Awake, then used whenver the tank dies
	public GameObject explosionPrefab;

	float health;
	public float currentHealth {
		get {
			return health;
		}
		set {
			health = value;
			SetHealthUI();
		}
	}
	public bool dead;
	
	// The particle systhem that will play when the tank is destroyed
	private ParticleSystem m_ExplosionParticles;

	public bool SpawnEnemyOnDeath;
	public int AmountOfFutureEnemies;

	// Gets a reference to the Players Tophat
	public GameObject PlayerTopHat;

	// Gets a list of colors for the tophat
	public List<Material> TopHatMaterials;

	// This is used for when an enemy tank dies, and spawns at a spawnpoint
	public Spawnpoints spawnpoints;

	public Image healthProgbar;

	private void Awake() {

		// Instantiate the explosion prefab and get a reference to
		// the particle system on it
		m_ExplosionParticles = Instantiate(explosionPrefab).GetComponent<ParticleSystem>();

		// Disable the prefab so it can be activated when it's required
		m_ExplosionParticles.gameObject.SetActive(false);
		
		if (tag != "Player") {
			startingHealth = 1;
		}

		Respawn();
	}

	public void Respawn() {
		// When the tank is enabled, reset the tank's health and whether or not it's dead
		currentHealth = startingHealth;
		dead = false;
	}

	private void SetHealthUI() {
		if (gameObject.tag == "Player") {
			healthProgbar.fillAmount = currentHealth / startingHealth;
		}
	}

	public void TakeDamage(float amount) {
		// Reduce current health by the amount of damage done
		currentHealth -= amount;

		// Change the UI elements appropriately
		SetHealthUI();

		// If the current health is at or below zero and it has not yet been registered, call OnDeath
		if (currentHealth <= 0f && !dead) {
			OnDeath();
		}
	}

	private void OnDeath() {
		// Set the flag so that this function is only called once
		dead = true;

		// Spawns another tank is SpawnEnemyOnDeath is true
		if (AmountOfFutureEnemies > 0) {
			if (SpawnEnemyOnDeath == true) {
				AmountOfFutureEnemies--;
				spawnpoints.SpawnEnemy(gameObject);
			}
		}
		

		// move the instantiated explosion prefab to the tank's position and turn it on
		m_ExplosionParticles.transform.position = transform.position;
		m_ExplosionParticles.gameObject.SetActive(true);

		// Play the particle system of the tank exploding
		m_ExplosionParticles.Play();

		// Turn the tank off
		gameObject.SetActive(false);
	}
}
