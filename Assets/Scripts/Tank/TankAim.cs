using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAim : MonoBehaviour
{
	// The Layer for the ray to hit then get a hit location from
    LayerMask layerMask;

	// Assigns variable to usable value
	private void Awake() {
		layerMask = LayerMask.GetMask("Ground");
	}


	private void Update() {

		// Creates a ray from the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// How to read if the ray hit anything
		RaycastHit hit;

		// If it has hit something, it makes the player's tank look at the location where it hit
		if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask)) {
			transform.LookAt(hit.point);
		}
	}
}
