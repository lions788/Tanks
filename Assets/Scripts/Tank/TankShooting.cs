using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
	// Prefab of the shell
	public GameObject shell;
	//A Child of the tank where the shells are spawned
	public Transform fireTransform;
	// The force given to the shell when firing
	public float launchForce = 30f;

	// Cooldown variable
	public float cooldownTime = 0.5f;
	private float fireCooldown;

	public Image cooldownProgBar;

    // Update is called once per frame
    void Update()
    {
		// This is also where I would put the code to disable mouse clicking through the UI buttons, using Unitys super duper handy function that it doesn't have. 
		if (Input.GetButtonUp("Fire1") && fireCooldown <= 0) {
			Fire();
			fireCooldown = 0.5f;
		}
    }

	void FixedUpdate() {

		// Lowers the cooldown from last shot
		fireCooldown -= Time.fixedDeltaTime;

		// Changes the cooldown bar to match the progress of the cooldown
		cooldownProgBar.fillAmount = 1 - (fireCooldown * 2);
	}

	private void Fire() {
		// Create an instance of the shell and store a reference to its rigidbody
		GameObject shellInstance = Instantiate(shell, fireTransform.position, fireTransform.rotation);

		// Set the shell's velocity to the launch force in the fire
		// position's forward direction
		shellInstance.GetComponent<Rigidbody>().velocity = launchForce * fireTransform.forward;
		shellInstance.GetComponent<Shell>().sender = gameObject;
	}
}
