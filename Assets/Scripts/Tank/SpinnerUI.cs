using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnerUI : MonoBehaviour
{

	GameObject Player;
	GameObject closestEnemyTank;
	Vector3 closestDistance = new Vector3(99999f, 99999f, 99999f);

	Camera MainCam;

	public GameManager gameManager;

	Transform spinna;
	CanvasGroup trans;

	void Awake() {
		Player = GameObject.Find("Player Tank");
		MainCam = Camera.main;
		spinna = gameObject.transform.Find("Spinner");
		trans = spinna.GetComponent<CanvasGroup>();
	}

    // Update is called once per frame
    void FixedUpdate()
    {

		// Only updates spinner if game is running
		if (gameManager.gameState == GameManager.GameState.Playing) {

			// Updates spinner if there are enemies to point towards
			if (gameManager.Tanks.Count > 0) {
				trans.alpha = 1; // Sets the alpha levelto opaque

				// Loops through every tank in the world
				foreach (GameObject enemy in gameManager.Tanks) {

					// Makes sure it's active and its not the player
					if (enemy != Player && enemy.activeSelf == true) {

						// Gets the distance between the player and the looped tank
						var distance = new Vector3();
						distance.x = (Mathf.Abs(Player.transform.position.x) - Mathf.Abs(enemy.transform.position.x));
						distance.y = (Mathf.Abs(Player.transform.position.y) - Mathf.Abs(enemy.transform.position.y));
						distance.z = (Mathf.Abs(Player.transform.position.z) - Mathf.Abs(enemy.transform.position.z));

						// Compares distances to find the closest tank then sets the closestEnemyTank to the current looped tank
						if (Mathf.Abs(distance.x) < closestDistance.x && Mathf.Abs(distance.y) < closestDistance.y && Mathf.Abs(distance.z) < closestDistance.z) {
							closestDistance = new Vector3(Mathf.Abs(distance.x), Mathf.Abs(distance.y), Mathf.Abs(distance.z));
							closestEnemyTank = enemy;
						}
					}
				}
				if (closestEnemyTank != null) {
					Vector3 position = MainCam.WorldToScreenPoint(closestEnemyTank.transform.position);
					var angle = Mathf.Atan2(gameObject.transform.position.y - position.y, gameObject.transform.position.x - position.x) * 180 / Mathf.PI;
					
					var newRotation = Quaternion.Euler(0, 0, angle);

					gameObject.transform.rotation = newRotation;
				}

				closestDistance = new Vector3(99999f, 99999f, 99999f);
				closestEnemyTank = null;
			}
		}
		else {
 			trans.alpha = 0;
		}
    }
}
