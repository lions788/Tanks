using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	// Current elapsed time
	public float gameTime = 0;

	// Game state enum
	public enum GameState {
		Start,
		Playing,
		GameOver
	};

	// Variable to hold the current game state
	public GameState gameState;
	public GameState State { get { return gameState; } }

	// All active tanks in the scene (player and enemy)
	public List<GameObject> Tanks;

	// Wave number
	int WaveNumber = 1;
	
	// UI Element References
	public ScoreManager scoreManager;
	GameObject MainUI;
	GameObject StartUI;
	public GameObject Scoreboard;
	public Text MessageText;
	public Text TimerText;
	public Text WaveText;
	public Text WaveTextMain;

	// Other references
	public Spawnpoints spawnpoints;

	void Awake() {
		// Set gamestate to start
		gameState = GameState.Start;
	}

	void Start() {
		// Disable all tanks and show start screen
		for (int i = 0; i < Tanks.Count; i++) {
			Tanks[i].SetActive(false);
		}

		MainUI = GameObject.Find("MainUI");
		StartUI = GameObject.Find("StartUI");
		Scoreboard = GameObject.Find("Scoreboard");

		StartUI.SetActive(true);
		MainUI.SetActive(false);
		Scoreboard.SetActive(false);
		WaveText.gameObject.SetActive(false);
		scoreManager.LoadScores();
	}

	void Update() {
		// Switch statement for managing game state
		switch (gameState) {
			case GameState.Start:
				if (Input.GetKeyUp(KeyCode.Return)) {
					TimerText.gameObject.SetActive(true);

					for (int i = 0; i < Tanks.Count; i++) {
						Tanks[i].SetActive(true);
					}

					// Switch active ui
					StartUI.SetActive(false);

					MainUI.SetActive(true);
					Scoreboard.SetActive(false);
					WaveText.gameObject.SetActive(false);
					WaveTextMain.text = $"Wave {WaveNumber}";
					gameState = GameState.Playing;
				}
				break;
			case GameState.Playing:
				bool isGameOver = false;

				gameTime += Time.deltaTime;
				int seconds = (int)gameTime;
				TimerText.text = string.Format("{0:D2}:{1:D2}",
												(seconds / 60), (seconds % 60));

				if (OneTankLeft())
					isGameOver = true;
				
				else if (IsPlayerDead())
					isGameOver = true;

				if (isGameOver) {
					gameState = GameState.GameOver;
					MainUI.gameObject.SetActive(false);
					StartUI.gameObject.SetActive(true);
 
					if (IsPlayerDead()) {
						MessageText.text = "ALL YOUR BASE ARE BELONG TO US";
						WaveText.gameObject.SetActive(true);
						WaveText.text = $"Died At Wave {WaveNumber}";
						WaveNumber = 0;
					}
					else {
						MessageText.text = "YOU'RE WINNER!";
						WaveText.gameObject.SetActive(true);
						WaveText.text = $"Completed Wave {WaveNumber}";
						scoreManager.AddToScores(seconds);
						scoreManager.SaveScores();
						WaveNumber++;
						WaveTextMain.text = $"Wave {WaveNumber}";
					}
				}
				
				break;
			// On restart game
			case GameState.GameOver:
				if (Input.GetKeyUp(KeyCode.Return)) {
					gameTime = 0;
					gameState = GameState.Playing;
					MessageText.text = "";
					MainUI.SetActive(true);
					StartUI.SetActive(false);
					WaveTextMain.text = $"Wave {WaveNumber}";

					for (int i = 0; i < Tanks.Count; i++)
					{
						Tanks[i].SetActive(true);
						Tanks[i].GetComponent<TankHealth>().Respawn();

						if (Tanks[i].tag == "Player") {
							Tanks[i].transform.position = new Vector3(0f, 0.1f, 0f);
							Tanks[i].transform.rotation = Quaternion.Euler(0f, 0f, 0f);
							Tanks[i].GetComponent<TankHealth>().startingHealth -= 5;
							if (Tanks[i].GetComponent<TankHealth>().startingHealth < 1) {
								Tanks[i].GetComponent<TankHealth>().startingHealth = 1;
							}
							Tanks[i].GetComponent<TankHealth>().currentHealth = Tanks[i].GetComponent<TankHealth>().startingHealth;
						} else {
							int randomSpawnpoint = Random.Range(0, spawnpoints.SpawnpointsList.Count);
							Transform spawnpoint = spawnpoints.SpawnpointsList[randomSpawnpoint];
							Tanks[i].transform.position = spawnpoint.transform.position;
						}
					}
				}
				break;
		}
		// Close game on escape
		if (Input.GetKeyUp(KeyCode.Escape)) {
			Application.Quit();
		}
	}

	// Returns true if only one tank is left
	private bool OneTankLeft() {
		int numTanksLeft = 0;

		for (int i = 0; i < Tanks.Count; i++) {
			if (Tanks[i].activeSelf) {
				numTanksLeft++;
			}
		}

		return numTanksLeft <= 1;
	}

	// Returns true if the player is dead
	private bool IsPlayerDead() {
		for (int i = 0; i < Tanks.Count; i++)
		{
			if (Tanks[i].activeSelf)
			{
				if (Tanks[i].tag == "Player")
					return false;
			}
		}
		return true;
	}
}
