using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Written by Rafael
public class ScoreManager : MonoBehaviour {
	public List<float?> scores = new List<float?>();
	public List<Text> scoreLabels;
	public Button scoresButton;
	public Button backButton;
	public Button resetButton;
	public bool resetProtected = true;
	public GameObject StartUI;
	public int maxSavedScores = 5;
	static string saveFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ShittyTonkGaem");
	static string saveFileName = "scores.save";
	string fullSaveFilePath = Path.Combine(saveFilePath, saveFileName);

	void Awake() {
		// Bind button click events. Theoretically you can do this through the unity UI but I couldn't figure out how
		scoresButton.onClick.AddListener(ShowScores);
		backButton.onClick.AddListener(HideScores);
		resetButton.onClick.AddListener(ResetScores);
		Init();
	}

	// Update and show all score labels
	public void ShowScores() {
		StartUI.SetActive(false);
		gameObject.SetActive(true);
		UpdateScores();
	}

	// Read scores from memory and show on the scoreboard labels
	public void UpdateScores() {
		// Clear score labels
		for (int i = 0; i < maxSavedScores; i++) {
			scoreLabels[i].text = "";
		}
		// Load scores and assign to labels
		for (int i = 0; i < scores.Count; i++) {
			scoreLabels[i].text = scores[i].ToString() + " seconds";
			scoreLabels[i].gameObject.SetActive(true);
		}
		// Re-lock the reset button
		resetProtected = true;
		resetButton.GetComponentInChildren<Text>().text = "Reset Scores";
	}

	// Hide all score labels
	public void HideScores() {
		gameObject.SetActive(false);
		StartUI.SetActive(true);
	}

	public void AddToScores(int newScore) {
		for (int i = 0; i < maxSavedScores; i++) {
			// Add new score to list if its better than another score
			if (scores.Count > i && newScore < scores[i]) {
				scores.Insert(i, newScore);
				break;
			// Else add it to the end of the list
			} else if (scores.Count < i) {
				scores.Add(newScore);
				break;
			}
		}
		// Clamp list length at maxSavedScores
		for (int j = maxSavedScores; j < scores.Count; j++) {
			scores.RemoveAt(scores.Count - 1);
		}
	}

	public void SaveScores() {
		// Serializes scores list to semicolon delimited string
		string scoresString = "";
		foreach (var score in scores) {
			scoresString += score.ToString() + ";";
		}
		// Encrypts the string and writes it to the save file
		var encryptedScores = CryptoManager.Encrypt(scoresString);
		File.WriteAllBytes(fullSaveFilePath, encryptedScores);
	}

	public void ResetScores() {
		// If the player clicked reset once, ask them to confirm
		if (resetProtected) {
			resetProtected = false;
			resetButton.GetComponentInChildren<Text>().text = "Are you sure? (click to confirm)";
		// If they clicked twice, actually reset them
		} else {
			scores.Clear();
			SaveScores();
			UpdateScores();
		}
	}

	public void LoadScores() {
		// Read all data from the save file and decrypts it to plaintext
		byte[] savedata = File.ReadAllBytes(fullSaveFilePath);
		string decryptedSaveData = CryptoManager.Decrypt(savedata);
		// Loads decrypted data into list
		List<float?> newScores = new List<float?>();
		foreach (var score in decryptedSaveData.Split(';')) {
			if (float.TryParse(score, out float _)) {
				newScores.Add(float.Parse(score));
			}
		}
		// Set new scores list to main scores list
		scores = newScores;
	}

	private void Init() {
		// Create save file if it doesn't exist
		if (!File.Exists(fullSaveFilePath)) {
			Directory.CreateDirectory(saveFilePath);
			File.Create(fullSaveFilePath);
		}
	}
}