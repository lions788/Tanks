using System.Text;

static class Key {
	// Must be 32 characters long AND CANNOT BE BLANK (learned that the hard way)
	private static string data = "hihihihihihihihihihihihihihihihi";

	// Converts data into key format
	public static byte[] key {
		get {
			return StringToBytes(data, 32);
		}
	}

	// Converts data into iv format
	public static byte[] iv {
		get {
			return StringToBytes(data, 16);
		}
	}

	// Converts string to bytearray of length `len`
	static byte[] StringToBytes(string str, int len) {
		string longdata = "";
		while (longdata.Length < len) {
			longdata += data;
		}
		return Encoding.Default.GetBytes(longdata.Substring(0, len));
	}
}