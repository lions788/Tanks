using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnpoints : MonoBehaviour
{

	// Makes a list of the optional spawnpoints within the game
	public List<Transform> SpawnpointsList = new List<Transform>();

	// Reference to gamemanager
	public GameManager myGameManager;

	void Start () {

		// Adds each spawnpoint (the child) to the list
		foreach (Transform child in transform) {
			SpawnpointsList.Add(child);
		}
	}


	public void SpawnEnemy(GameObject Enemy) {
		// Gets random index number to get the spawnpoint at that index in the list
		int randomSpawnpoint = Random.Range(0, SpawnpointsList.Count);

		// Gets object from list at that index
		Transform spawnpoint = SpawnpointsList[randomSpawnpoint];

		// Spawns a new tank at the location
		GameObject NewTank = Instantiate(Enemy, spawnpoint.transform.position, spawnpoint.transform.rotation);
		NewTank.GetComponent<TankHealth>().Respawn();
		
		// Adds the newly spawned tank to the gamemanagers list 
		myGameManager.Tanks.Add(NewTank);
	}
}