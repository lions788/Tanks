using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTracker : MonoBehaviour
{
	// The Layer for the ray to hit then get a hit location from
	LayerMask m_LayerMask;

	public GameObject Player;
	public float radius;

	// Assigns variable to usable value
	private void Awake() {
		m_LayerMask = LayerMask.GetMask("Ground");
	}


	private void Update() {

		// Creates a ray from the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// How to read if the ray hit anything
		RaycastHit hit;

		// If it has hit something, it makes the player's tank look at the location where it hit
		if (Physics.Raycast(ray, out hit, Mathf.Infinity, m_LayerMask)) {
			
			var newPos = hit.point;

			// Sets the center point for the clamp
			Vector3 centerPt = Player.transform.position;

			// Calculate the distance of the new position from the center point. Keep the direction
			// the same but clamp the length to the specified radius.
   			Vector3 offset = newPos - centerPt;
			transform.position = centerPt + Vector3.ClampMagnitude(offset, radius);
		}
	}	
}
